import "./index.css";
import {
  b2World,
  b2Vec2,
  b2BodyDef,
  b2BodyType,
  b2PolygonShape,
  b2ShapeType,
  b2CircleShape,
  b2Vec2_zero
} from "@flyover/box2d";
import resizeCanvas from "./resizeCanvas";

import createBatch from "gdxjs/lib/createBatch";
import createCam from "gdxjs/lib/orthoCamera";
import createWhiteTex from "gl-white-texture";

import { drawPolygon, fillRect } from "./shapeUtil";

const info = document.getElementById("info");
const canvas = document.getElementById("main");
const [width, height] = resizeCanvas(canvas, 1);

const worldWidth = 100;
const worldHeight = (100 * height) / width;

const gl = canvas.getContext("webgl");

const batch = createBatch(gl);
const camera = createCam(worldWidth, worldHeight, width, height);
const whiteTex = createWhiteTex(gl);

const gravity = new b2Vec2(0, 100);
const world = new b2World(gravity);

const createBox = (x, y, width, height, dynamic = true, density = 1) => {
  const bodyDef = new b2BodyDef();
  bodyDef.type = dynamic ? b2BodyType.b2_dynamicBody : b2BodyType.b2_staticBody;
  bodyDef.position = new b2Vec2(x + width / 2, y + height / 2);
  const body = world.CreateBody(bodyDef);
  const shape = new b2PolygonShape();
  shape.SetAsBox(width / 2, height / 2);
  body.CreateFixture(shape, density);

  return body;
};

const createBall = (x, y, r, impulseX, impulseY) => {
  const bodyDef = new b2BodyDef();
  bodyDef.type = b2BodyType.b2_dynamicBody;
  bodyDef.position = new b2Vec2(x, y);
  const body = world.CreateBody(bodyDef);
  const shape = new b2CircleShape(r);
  body.CreateFixture(shape, 1);
  body.ApplyLinearImpulse(new b2Vec2(impulseX, impulseY), b2Vec2_zero);
  body.SetUserData({
    expire: 1,
    timeSpan: 0
  });
  return body;
};

createBox(10, worldHeight - 10, worldWidth - 20, 5, false);
createBox(worldWidth / 2 - 5, 10, 10, 50, true, 1000);

const tmp = [];
gl.clearColor(0, 0, 0, 1);
const update = delta => {
  gl.clear(gl.COLOR_BUFFER_BIT);
  createBall(
    20,
    worldHeight - 30,
    1,
    Math.random() * 1000,
    Math.random() * 1000
  );
  if (Math.random() <= 0.03) {
    const box = createBox(30, 30, 3, 3, true, 1);
    box.SetUserData({
      expire: 10,
      timeSpan: 0
    });
  }
  for (let body = world.GetBodyList(); body; body = body.GetNext()) {
    const data = body.GetUserData();
    if (data && data.expire) {
      data.timeSpan += delta;
      if (data.timeSpan >= data.expire) {
        world.DestroyBody(body);
      }
    }
  }
  world.Step(delta, 8, 3);
  batch.setProjection(camera.combined);
  batch.begin();

  for (let body = world.GetBodyList(); body; body = body.GetNext()) {
    const pos = body.GetPosition();
    for (
      let fixture = body.GetFixtureList();
      fixture;
      fixture = fixture.GetNext()
    ) {
      const shape = fixture.GetShape();
      if (shape.GetType() === b2ShapeType.e_polygonShape) {
        const vertices = shape.m_vertices;
        tmp.length = 0;
        for (let vertice of vertices) {
          tmp.push(vertice.x + pos.x, vertice.y + pos.y);
        }
        drawPolygon(batch, whiteTex, tmp);
      } else if (shape.GetType() === b2ShapeType.e_circleShape) {
        fillRect(batch, whiteTex, pos.x, pos.y, shape.m_radius, shape.m_radius);
      }
    }
  }

  batch.end();
};

let lastUpdate = Date.now();
let fps = 0;
(function loop() {
  const delta = Date.now() - lastUpdate;
  lastUpdate = Date.now();
  fps = Math.round(1000 / delta);
  update(delta / 1000);
  requestAnimationFrame(loop);
})();

setInterval(() => {
  info.innerHTML = `FPS: ${fps} - Body Count: ${world.GetBodyCount()}`;
}, 1000);
