import "./index.css";

import {
  b2World,
  b2BodyDef,
  b2BodyType,
  b2Vec2,
  b2ShapeType,
  b2PolygonShape,
  b2Vec2_zero,
  b2CircleShape,
  b2FixtureDef
} from "@flyover/box2d";
import resizeCanvas from "./resizeCanvas";

import loadTexture from "gdxjs/lib/loadTexture";
import createBatch from "gdxjs/lib/createBatch";
import createCam from "gdxjs/lib/orthoCamera";
import createWhiteTex from "gl-white-texture";
import InputHandler from "gdxjs/lib/InputHandler";

import { drawPolygon, fillRect } from "./shapeUtil";

const init = async () => {
  const info = document.getElementById("info");

  const canvas = document.getElementById("main");
  const [width, height] = resizeCanvas(canvas, 1);

  const worldHeight = 3;
  const worldWidth = (worldHeight * width) / height;

  const gl = canvas.getContext("webgl");

  const batch = createBatch(gl);
  const camera = createCam(worldWidth, worldHeight, width, height);
  const whiteTex = createWhiteTex(gl);
  const circleTex = await loadTexture(gl, "./circle.png");

  const world = new b2World(new b2Vec2(0, 10));

  const inputHandler = new InputHandler(canvas);

  let touched = false;

  const screenCoords = [0, 0];
  const worldCoords = [0, 0];
  inputHandler.addEventListener("touchStart", (x, y) => {
    touched = true;
    screenCoords[0] = x;
    screenCoords[1] = y;
    camera.unprojectVector2(worldCoords, screenCoords);
  });
  inputHandler.addEventListener("touchMove", (x, y) => {
    if (touched) {
      screenCoords[0] = x;
      screenCoords[1] = y;
      camera.unprojectVector2(worldCoords, screenCoords);
    }
  });
  inputHandler.addEventListener("touchEnd", (x, y) => {
    touched = false;
  });

  const createBox = (x, y, width, height, dynamic = true, density = 1) => {
    const bodyDef = new b2BodyDef();
    bodyDef.type = dynamic
      ? b2BodyType.b2_dynamicBody
      : b2BodyType.b2_staticBody;
    bodyDef.position = new b2Vec2(x + width / 2, y + height / 2);
    const body = world.CreateBody(bodyDef);
    const shape = new b2PolygonShape();
    shape.SetAsBox(width / 2, height / 2);
    const fixtureDef = new b2FixtureDef();
    fixtureDef.density = density;
    fixtureDef.shape = shape;
    fixtureDef.friction = 1;
    body.CreateFixture(fixtureDef);
    return body;
  };

  const createBall = (x, y, r, impulseX, impulseY) => {
    const bodyDef = new b2BodyDef();
    bodyDef.type = b2BodyType.b2_dynamicBody;
    bodyDef.position = new b2Vec2(x, y);
    bodyDef.fixedRotation = true;
    const body = world.CreateBody(bodyDef);
    const shape = new b2CircleShape(r);
    const fixture = body.CreateFixture(shape, 1);
    body.ApplyLinearImpulse(new b2Vec2(impulseX, impulseY), b2Vec2_zero);
    fixture.SetFriction(1);
    return body;
  };

  const ground = createBox(0.1, 2.5, worldWidth - 0.2, 0.2, false);
  const box = createBox(worldWidth / 2 - 0.1, worldHeight / 2 - 0.1, 0.2, 0.2);

  let accumulate = 0;
  const tmp = [];
  gl.clearColor(0, 0, 0, 1);
  const update = delta => {
    gl.clear(gl.COLOR_BUFFER_BIT);

    accumulate += delta;
    while (accumulate >= 1) {
      createBall(0.2, 2.3, 0.1, Math.random() * 0.2, Math.random() * -0.2);
      accumulate -= 1;
    }
    if (touched) {
      createBox(worldCoords[0] - 0.1, worldCoords[1] - 0.1, 0.2, 0.2);
    }

    for (let body = world.GetBodyList(); body; body = body.GetNext()) {
      const pos = body.GetPosition();
      if (pos.x < 0 || pos.x > worldWidth || pos.y < 0 || pos.y > worldHeight) {
        world.DestroyBody(body);
      }
    }
    world.Step(delta, 8, 3);

    batch.setProjection(camera.combined);
    batch.begin();

    for (let body = world.GetBodyList(); body; body = body.GetNext()) {
      const pos = body.GetPosition();
      for (
        let fixture = body.GetFixtureList();
        fixture;
        fixture = fixture.GetNext()
      ) {
        const shape = fixture.GetShape();
        if (shape.GetType() === b2ShapeType.e_polygonShape) {
          const vertices = shape.m_vertices;
          tmp.length = 0;
          for (let vertice of vertices) {
            tmp.push(vertice.x + pos.x, vertice.y + pos.y);
          }
          drawPolygon(batch, whiteTex, tmp, 0.01);
        } else if (shape.GetType() === b2ShapeType.e_circleShape) {
          batch.draw(
            circleTex,
            pos.x - shape.m_radius,
            pos.y - shape.m_radius,
            shape.m_radius * 2,
            shape.m_radius * 2,
            shape.m_radius,
            shape.m_radius,
            body.GetAngle()
          );
        }
      }
    }

    batch.end();
  };

  let lastUpdate = Date.now();
  let fps = 0;
  (function loop() {
    const delta = Date.now() - lastUpdate;
    fps = Math.round(1000 / delta);
    lastUpdate = Date.now();

    update(delta / 1000);
    requestAnimationFrame(loop);
  })();

  setInterval(
    () =>
      (info.innerHTML = `FPS: ${fps} - Body count: ${world.GetBodyCount()}`),
    1000
  );
};

init();
