const resize = (canvas, pixelRatio) => {
  const devicePixelRatio = pixelRatio || window.devicePixelRatio || 1;

  const displayWidth = canvas.clientWidth * devicePixelRatio;
  const displayHeight = canvas.clientHeight * devicePixelRatio;

  if (canvas.width !== displayWidth || canvas.height !== displayHeight) {
    canvas.width = displayWidth;
    canvas.height = displayHeight;
  }
  return [displayWidth, displayHeight];
};

export default resize;
