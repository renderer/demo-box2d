import Vector2 from "gdxjs/lib/vector2";

const tmpV1 = new Vector2(0, 0);
const tmpV2 = new Vector2(0, 0);
export const drawLine = (
  batch,
  tex,
  x1,
  y1,
  x2,
  y2,
  thickness = 1,
  r = 1,
  g = 1,
  b = 1,
  a = 1
) => {
  tmpV1.set(x1, y1);
  tmpV2.set(x2, y2);
  const length = tmpV1.distance(tmpV2);
  const angle = tmpV2.subVector(tmpV1).angle();
  batch.setColor(r, g, b, a);
  batch.draw(tex, x1, y1, thickness * 0.5, length, 0, 0, angle - Math.PI / 2);
  batch.setColor(1, 1, 1, 1);
};

export const drawPolygon = (
  batch,
  tex,
  verts,
  thickness = 1,
  r = 1,
  g = 1,
  b = 1,
  a = 1
) => {
  if (verts.length < 4) {
    return;
  }
  let prevX = verts[0];
  let prevY = verts[1];
  for (let i = 2; i < verts.length; i++) {
    const x = verts[i];
    const y = verts[++i];

    drawLine(batch, tex, prevX, prevY, x, y, thickness, r, g, b, a);

    prevX = x;
    prevY = y;
  }
  drawLine(batch, tex, prevX, prevY, verts[0], verts[1], thickness, r, g, b, a);
};

export const fillRect = (
  batch,
  tex,
  x,
  y,
  w,
  h,
  rotation = 0,
  originX = w / 2,
  originY = h / 2
) => {
  batch.draw(tex, x, y, w, h, originX, originY, rotation);
};
